package com.catnyx.store;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.textfield.TextInputLayout;
import com.google.firebase.FirebaseException;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.PhoneAuthCredential;
import com.google.firebase.auth.PhoneAuthProvider;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

public class RegisterUserActivity extends AppCompatActivity {
    EditText username,mobile,email,pwd,c_pwd,otp;
    ProgressBar loading,verify;
    Button register;
    TextView login;
    RequestQueue mqueue;
    SharedPreferences prefs;
    SharedPreferences.Editor editor;
    String verification_code;
    TextInputLayout field;
    FirebaseAuth mFirebaseAuth;
    PhoneAuthCredential credential;
    String mobileforFirebase;
    PhoneAuthProvider.OnVerificationStateChangedCallbacks mCallbacks;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register_user);
        username=findViewById(R.id.username);
        mobile=findViewById(R.id.mobile);
        email=findViewById(R.id.email);
        pwd=findViewById(R.id.pwd);
        c_pwd=findViewById(R.id.c_pwd);
        loading=findViewById(R.id.loading);
        login=findViewById(R.id.login);
        otp=findViewById(R.id.otp);
        otp.setVisibility(View.INVISIBLE);
        mqueue= Volley.newRequestQueue(getApplicationContext());
        register=findViewById(R.id.register);
        verify=findViewById(R.id.verify);
        verify.setVisibility(View.INVISIBLE);
        field=findViewById(R.id.field);
        field.setVisibility(View.INVISIBLE);


        prefs=getSharedPreferences("user",MODE_PRIVATE);
        editor=prefs.edit();
        mFirebaseAuth=FirebaseAuth.getInstance();
        login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(RegisterUserActivity.this,LoginActivity.class));
            }
        });
        register.setTag("SEND");

        mCallbacks = new PhoneAuthProvider.OnVerificationStateChangedCallbacks() {
            @Override
            public void onVerificationCompleted(@NonNull PhoneAuthCredential phoneAuthCredential) {
                Toast.makeText(RegisterUserActivity.this,"Verification Completed",Toast.LENGTH_SHORT).show();
                String code=phoneAuthCredential.getSmsCode();
                if(code!=null){
                    otp.setText(code);
                    verifyCode(code);
                }
            }

            @Override
            public void onVerificationFailed(@NonNull FirebaseException e) {
                Toast.makeText(RegisterUserActivity.this,"Verification Didnt Finish! Error",Toast.LENGTH_SHORT).show();
                e.printStackTrace();
            }

            @Override
            public void onCodeSent(@NonNull String s, @NonNull PhoneAuthProvider.ForceResendingToken forceResendingToken) {
                super.onCodeSent(s, forceResendingToken);
                verification_code=s;
                otp.setVisibility(View.VISIBLE);
                verify.setVisibility(View.INVISIBLE);
                field.setVisibility(View.VISIBLE);
            }
        };

        register.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (register.getTag().equals("SEND")) {
                    if (TextUtils.isEmpty(username.getText().toString())) {
                        username.setError("Please enter name");
                        username.requestFocus();
                    } else if (TextUtils.isEmpty(mobile.getText().toString())) {
                        mobile.setError("Please enter mobile");
                        mobile.requestFocus();
                    } else if (TextUtils.isEmpty(email.getText().toString())) {
                        email.setError("Please enter email");
                        email.requestFocus();
                    } else if (TextUtils.isEmpty(pwd.getText().toString())) {
                        pwd.setError("Please enter password");
                        pwd.requestFocus();
                    } else if (pwd.getText().toString().length() < 8) {
                        pwd.setError("Password not valid");
                        pwd.requestFocus();
                    } else if (!pwd.getText().toString().equals(c_pwd.getText().toString())) {
                        c_pwd.setError("Password not valid");
                        c_pwd.requestFocus();
                    } else {
                        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                        imm.hideSoftInputFromWindow(register.getWindowToken(), 0);
//                        register.setVisibility(View.INVISIBLE);
//                        loading.setVisibility(View.VISIBLE);
                        otp.setVisibility(View.VISIBLE);
                        verify.setVisibility(View.VISIBLE);
                        register.setTag("REGISTER");
                        mobileforFirebase="+91"+mobile.getText().toString();
                        Log.i("MOBILE:",mobileforFirebase);
//                    registerUser();
                        PhoneAuthProvider.getInstance().verifyPhoneNumber(
                                mobileforFirebase,
                                120,
                                TimeUnit.SECONDS,
                                RegisterUserActivity.this,
                                mCallbacks
                        );
                        register.setText("Sign Up");
                    }
                }else{
                    register.setTag("SEND");
                    Toast.makeText(getApplicationContext(),"OTP",Toast.LENGTH_SHORT).show();
                    verifyCode(otp.getText().toString());
                }
            }
        });



    }


    private void verifyCode(String code){
        credential=PhoneAuthProvider.getCredential(verification_code,code);
        mFirebaseAuth.signInWithCredential(credential).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {
                if (task.isSuccessful()){
//                createUserWithCredential(credential);
                    Toast.makeText(getApplicationContext(),"Success!",Toast.LENGTH_SHORT).show();
                    registerUser();
                    mFirebaseAuth.signOut();
                }else {
                    Toast.makeText(getApplicationContext(),"Wrong OTP!",Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    public void registerUser(){
        StringRequest request=new StringRequest(Request.Method.POST, ConstantClass.REGISTER, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
//                Log.i("STATUS","done");
                register.setVisibility(View.VISIBLE);
                loading.setVisibility(View.INVISIBLE);
                try {
                    JSONObject object=new JSONObject(response);
                    JSONObject data=object.getJSONObject("success");
                    String token=data.getString("token");
                    String name=data.getString("name");
                    String id=data.getString("id");
                    String get_email=email.getText().toString();
                    editor.putString("email",get_email);
                    editor.putString("token",token);
                    editor.putString("name",name);
                    editor.putString("id",id);
                    editor.apply();
                    finish();
                    startActivity(new Intent(RegisterUserActivity.this,HomeActivity.class));
                } catch (Exception e) {
                    e.printStackTrace();
                }

//                Log.i("RESULT",response.toString());
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
                register.setVisibility(View.VISIBLE);
                loading.setVisibility(View.INVISIBLE);
            }
        }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                HashMap<String,String> umap=new HashMap<>();
                umap.put("name",username.getText().toString().trim());
                umap.put("email",email.getText().toString().trim());
                umap.put("c_password",c_pwd.getText().toString().trim());
                umap.put("contact",mobile.getText().toString().trim());
                umap.put("password",pwd.getText().toString().trim());
                return umap;
            }
        };

        mqueue.add(request).setRetryPolicy(new RetryPolicy() {
            @Override
            public int getCurrentTimeout() {
                return 5000;
            }

            @Override
            public int getCurrentRetryCount() {
                return 5000;
            }

            @Override
            public void retry(VolleyError error) throws VolleyError {
                Toast.makeText(getApplicationContext(),"Email already exists!",Toast.LENGTH_SHORT).show();
                register.setVisibility(View.VISIBLE);
                loading.setVisibility(View.INVISIBLE);
            }
        });
    }
}