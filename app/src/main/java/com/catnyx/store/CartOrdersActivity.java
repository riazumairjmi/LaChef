package com.catnyx.store;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.razorpay.Checkout;
import com.razorpay.PaymentResultListener;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CartOrdersActivity extends AppCompatActivity implements PaymentResultListener {
    RecyclerView cart_items;
    List<BillDetailsClass> itemList;
    CartAdapter adapter;
    EditText offer;
    TextView total;
    Button order;
    TextView offer_btn;
    RequestQueue mqueue;
    boolean coupon=false;
    RadioGroup payment_type;
    ProgressBar loading;
    RadioButton rbtn;
    String offer_text;
    String cashOrCard="Cash";
    int discount=0;
    SharedPreferences prefs;
    int cost_after_discount;
    int cost_before_discount;
    String payment_status="fail";
    SharedPreferences.Editor editor;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cart_orders);
        cart_items=findViewById(R.id.cart_items);
        total=findViewById(R.id.total);
        offer=findViewById(R.id.offer);
        offer_btn=findViewById(R.id.offer_btn);
        payment_type=findViewById(R.id.payment_type);
        prefs=getSharedPreferences("user", Context.MODE_PRIVATE);
        editor=prefs.edit();
        loading=findViewById(R.id.loading);
        loading.setVisibility(View.VISIBLE);
        itemList=new ArrayList<>();
//        ConstantClass.RESTAURANT_ID=prefs.getString("cart_number","0");
//        Log.i("RESTAURANT_ID",ConstantClass.RESTAURANT_ID);
        Intent intent=getIntent();
        String id=intent.getStringExtra("category_id");
//        Log.i("IDDDDD",id+"IDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDD");
        ConstantClass.RESTAURANT_ID=id;
        adapter=new CartAdapter(this,itemList);
        order=findViewById(R.id.order_now);
        payment_type.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                rbtn=findViewById(checkedId);
                cashOrCard=rbtn.getText().toString();
                if(cashOrCard.equals("Cash")){
                    payment_status="fail";
                }else{
                    payment_status="success";
                }
            }
        });

        offer_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(offer_btn.getWindowToken(), 0);
                offer_text=offer.getText().toString();
                cost_before_discount=cost_after_discount;
                if(!coupon) {
                    StringRequest request = new StringRequest(Request.Method.POST, ConstantClass.OFFERS_TEXT, new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            try {
                                JSONObject object = new JSONObject(response);
                                Toast.makeText(getApplicationContext(), object.getString("message"), Toast.LENGTH_SHORT).show();
                                if (object.getString("status").equals("200")) {
                                    discount = Integer.parseInt(object.getString("discount"));
                                    cost_after_discount = cost_after_discount - (cost_after_discount * discount) / 100;
                                    coupon = true;
                                    total.setText(String.valueOf(cost_after_discount));
                                } else if (object.getString("status").equals("404")) {
                                    Toast.makeText(getApplicationContext(), "Coupon Does not Match!", Toast.LENGTH_SHORT).show();
                                    total.setText(String.valueOf(cost_before_discount));
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    }, new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            error.printStackTrace();
                            total.setText(String.valueOf(cost_before_discount));
                        }
                    }) {

                        @Override
                        protected Map<String, String> getParams() throws AuthFailureError {
                            HashMap<String, String> umap = new HashMap<>();
                            umap.put("coupon_code", offer_text);
                            return umap;
                        }

                        @Override
                        public Map<String, String> getHeaders() throws AuthFailureError {
                            HashMap<String, String> map = new HashMap<>();
                            map.put("Authorization", "Bearer " + ConstantClass.TOKEN);
                            return map;
                        }
                    };
                    mqueue.add(request);
                }else{
                    Toast.makeText(getApplicationContext(), "Coupon Already Applied!", Toast.LENGTH_SHORT).show();
                }
            }
        });
        cart_items.setLayoutManager(new LinearLayoutManager(this));
        cart_items.setAdapter(adapter);
        mqueue= Volley.newRequestQueue(this);
        order.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new AlertDialog.Builder(CartOrdersActivity.this)
                        .setTitle("Confirm Order")
                        .setMessage("Are you sure you want to confirm order with the current address?")
                        .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                Log.i("STRIII:",prefs.getString("state","0")
                                        +prefs.getString("city","0")
                                        + prefs.getString("mobile","0")
                                        +prefs.getString("address","0")
                                        +prefs.getString("email","0")
                                        +prefs.getString("zip","0")
                                );
                                if(prefs.getString("address","0").equals("0")){
                                    Toast.makeText(CartOrdersActivity.this,"Please update address!",Toast.LENGTH_LONG).show();
                                }else{
                                    if(cashOrCard.equals("Cash")){
                                        confirmOrder();
                                    }else{
                                        confirmCard();
                                    }
                                    Log.i("TYPEPAY",cashOrCard);
                                }
                            }
                        })
                        .setNegativeButton("No",null)
                        .show();
            }
        });
    }

    public void confirmOrder(){
        StringRequest request = new StringRequest(Request.Method.POST, ConstantClass.CONFIRM_ORDER, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.i("RESPONSE",response);
                try {
                    JSONObject object=new JSONObject(response);
                    Toast.makeText(CartOrdersActivity.this,object.getString("message"),Toast.LENGTH_SHORT).show();
                    editor.putString("cart_available","no");
                    Intent intent=new Intent(CartOrdersActivity.this,MyOrdersActivity.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
                Toast.makeText(CartOrdersActivity.this,"No Items in Cart",Toast.LENGTH_SHORT).show();
            }
        }){

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                HashMap<String,String> umap=new HashMap<>();
                umap.put("state",prefs.getString("state","0"));
                umap.put("city",prefs.getString("city","0"));
                umap.put("email",prefs.getString("email","0"));
                umap.put("pin",prefs.getString("zip","0"));
                umap.put("address",prefs.getString("address","0"));
                umap.put("mobile",prefs.getString("mobile","0"));
                umap.put("category_id",ConstantClass.RESTAURANT_ID);
                umap.put("payment_mode",cashOrCard);
                umap.put("discount",String.valueOf(discount));
                umap.put("payment_status",payment_status);
                return umap;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String,String> map=new HashMap<>();
                map.put("Authorization","Bearer "+ConstantClass.TOKEN);
                return map;
            }
        };
        mqueue.add(request).setRetryPolicy(new RetryPolicy() {
            @Override
            public int getCurrentTimeout() {
                return 5000;
            }

            @Override
            public int getCurrentRetryCount() {
                return 5000;
            }

            @Override
            public void retry(VolleyError error) throws VolleyError {

            }
        });
    }

    public void getCartItems(){
        Log.i("RES",ConstantClass.CART_ITEMS+ConstantClass.RESTAURANT_ID);
        StringRequest request=new StringRequest(Request.Method.GET, ConstantClass.CART_ITEMS+ConstantClass.RESTAURANT_ID, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    Log.i("RES",response);
                    JSONObject object=new JSONObject(response);
                    JSONArray array=object.getJSONArray("data");
//                    String message=object.getString("message");
//                    Log.i("SIZE:",array.length()+"size");
                    itemList.clear();
                    for(int i=0;i<array.length();i++){
                        JSONObject obj=array.getJSONObject(i);
                        itemList.add(new BillDetailsClass(obj.getString("id"),ConstantClass.IMAGE_ACCESS+obj.getString("image"),obj.getString("quantity"),obj.getString("product_name"),obj.getString("subtotal"),obj.getString("rate")));
                    }
                    total.setText(object.getString("total"));
                    cost_after_discount=Integer.parseInt(object.getString("total"));
                    adapter.notifyDataSetChanged();
                    loading.setVisibility(View.INVISIBLE);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
            }
        }){
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String,String> map=new HashMap<>();
                map.put("Authorization","Bearer "+ConstantClass.TOKEN);
                return map;
            }
        };
        mqueue.add(request).setRetryPolicy(new RetryPolicy() {
            @Override
            public int getCurrentTimeout() {
                return 5000;
            }

            @Override
            public int getCurrentRetryCount() {
                return 5000;
            }

            @Override
            public void retry(VolleyError error) throws VolleyError {

            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        getCartItems();
    }

    public void confirmCard(){
        Checkout checkout=new Checkout();
        checkout.setKeyID(ConstantClass.TOKEN_RAZOR);
        checkout.setImage(R.drawable.rzp_name_logo);
        final Activity activity=this;
        try{
            JSONObject options=new JSONObject();
            options.put("name",prefs.getString("name","0"));
            options.put("description", "Reference No. #123456");
            options.put("image", "https://s3.amazonaws.com/rzp-mobile/images/rzp.png");
//          options.put("order_id", "order_DBJOWzybf0sJbb");//from response of step 3.
            options.put("theme.color", "#3399cc");
            options.put("currency", "INR");
            options.put("amount", String.valueOf(Integer.parseInt(total.getText().toString())*100));//pass amount in currency subunits
            options.put("prefill.email", prefs.getString("email","0"));
            options.put("prefill.contact",prefs.getString("contact","0"));
            checkout.open(activity,options);
        }catch (Exception e){
            Log.e("STATUS","Error");
            Toast.makeText(getApplicationContext(),"Payment Error!",Toast.LENGTH_LONG).show();
        }
    }

    @Override
    public void onPaymentSuccess(String s) {
//        Log.i("PAYMENT",s);
        Toast.makeText(getApplicationContext(),"Payment Success!",Toast.LENGTH_LONG).show();
        confirmOrder();
    }

    @Override
    public void onPaymentError(int i, String s) {
//        Log.i("PAYMENTERROR",s);
        Toast.makeText(getApplicationContext(),"Payment Error!",Toast.LENGTH_LONG).show();
    }
}