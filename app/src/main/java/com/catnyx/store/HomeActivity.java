package com.catnyx.store;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.widget.LinearLayout;

import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

public class HomeActivity extends AppCompatActivity {
    Fragment fragment;
    FragmentTransaction ft;
    LinearLayout container;
    private BottomNavigationView bottomNavigationView;
    SharedPreferences prefs;
    DatabaseReference test;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        container=findViewById(R.id.container);
        bottomNavigationView=findViewById(R.id.bottomNavigationView);
        prefs=getSharedPreferences("user",MODE_PRIVATE);
        fragment=HomeFragment.newInstance();
        createFragment(fragment);
//        if(prefs.getString("cart_available","no").equals("yes")){
//            fragment=ConfirmOrderFragment.newInstance();
//            createFragment(fragment);
//            bottomNavigationView.setSelectedItemId(R.id.cart);
//        }
        ConstantClass.TOKEN=prefs.getString("token","no");
        ConstantClass.USER_NAME=prefs.getString("name","no");
//        Log.i("TOKENVALUE",ConstantClass.TOKEN);
        bottomNavigationView.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                switch (item.getItemId()){
                    case R.id.home: fragment= HomeFragment.newInstance();
                    break;
                    case R.id.search: fragment= SearchFragment.newInstance();
                    break;
                    case R.id.cart: if(prefs.getString("cart_available","no").equals("yes")){
                        fragment= CartOrderCategoriesFragment.newInstance();
                    }else{
                        fragment=CartFragment.newInstance();
                    }
                    break;
                    case R.id.user: fragment=AccountFragment.newInstance();
                    break;
                }
                createFragment(fragment);
                return true;
            }
        });
    }

    public void createFragment(Fragment fragment){
        ft=getSupportFragmentManager().beginTransaction();
        ft.replace(R.id.container,fragment).commit();
    }
}