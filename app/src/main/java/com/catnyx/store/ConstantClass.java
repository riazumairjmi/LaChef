package com.catnyx.store;

public class ConstantClass {
    public static boolean isCart=false;
    public static final String LOGIN="https://lachef.humanmantra.website/api/user-login";
    public static final String REGISTER="https://lachef.humanmantra.website/api/user-register";
    public static final String RESTAURANTS="https://lachef.humanmantra.website/api/categories";
    public static final String IMAGE_ACCESS="https://lachef.humanmantra.website";
    public static String selected_restaurant_id="1";
    public static String selected_category_id="1";
    public static String selected_item_id="1";
    public static final String CATEGORY="https://lachef.humanmantra.website/api/sub-categories/";
    public static final String ITEM="https://lachef.humanmantra.website/api/subcategory-products/";
    public static final String USER_ACCOUNT="https://lachef.humanmantra.website/api/user-Info";
    public static String TOKEN="";
    public static final String ADD_ADDRESS="https://lachef.humanmantra.website/api/create-update-address";
    public static final String ADD_CART="https://lachef.humanmantra.website/api/add-to-cart";
    public static String RESTAURANT_ID="";
    public static String CATEGORY_NAME="";
    public static final String CART_ITEMS="https://lachef.humanmantra.website/api/cart-item-list-by-category-id/";
    public static final String REMOVE_CART_ITEM="https://lachef.humanmantra.website/api/delete-single-product-in-cart";
    public static final String CONFIRM_ORDER="https://lachef.humanmantra.website/api/checkout";
    public static final String MY_ORDERS="https://lachef.humanmantra.website/api/my-orders";
    public static final String SEARCH_QUERY="https://lachef.humanmantra.website/api/product/search/";
    public static final String ALL_SEARCH="https://lachef.humanmantra.website/api/all-products";

    public static final String CART_RESTAURANTS="https://lachef.humanmantra.website/api/cart-category-list";
    public static String SUB_IMAGE="";
    public static String USER_NAME="";

    public static final String TOKEN_RAZOR="rzp_test_S9eiUwfrsfwyAN";
    public static final String OFFERS_TEXT="https://lachef.humanmantra.website/api/apply-coupon";
    public static final String UPDATE_CART="https://lachef.humanmantra.website/api/update-cart";
    public static final String GET_VENDORS="https://lachef.humanmantra.website/api/categories-list-by-lat-long";
    public static String BANNER_IMAGES[]={};
    public static final String BANNER_URL="https://lachef.humanmantra.website/api/banners";
    public static boolean GIVE_LOCATION=false;
}
