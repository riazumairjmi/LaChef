package com.catnyx.store;

import android.app.Activity;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;

import java.util.List;

public class CartCategoryAdapter extends RecyclerView.Adapter<CartCategoryAdapter.MyViewHolder> {
    Activity mContext;
    List<Categories> categories;

    public CartCategoryAdapter(Activity mContext, List<Categories> categories) {
        this.mContext = mContext;
        this.categories = categories;
    }

    @NonNull
    @Override
    public CartCategoryAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(parent.getContext()).inflate(R.layout.search_query_layout,parent,false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull CartCategoryAdapter.MyViewHolder holder, final int position) {
        holder.name.setText(categories.get(position).getName());
        Glide.with(mContext)
                .load(categories.get(position).getImage())
                .centerCrop()
                .into(holder.img);
        holder.item.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(mContext,CartOrdersActivity.class);
                intent.putExtra("category_id",categories.get(position).getId());
                Log.i("CLICKED",categories.get(position).getId()+"sure");
                mContext.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return categories.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView name;
        ImageView img;
        LinearLayout item;
        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            name=itemView.findViewById(R.id.name);
            img=itemView.findViewById(R.id.img);
            item=itemView.findViewById(R.id.item);
        }
    }
}
