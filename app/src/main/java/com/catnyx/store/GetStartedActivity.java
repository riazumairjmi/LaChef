package com.catnyx.store;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.viewpager.widget.ViewPager;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.util.TypedValue;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.google.android.material.tabs.TabLayout;

public class GetStartedActivity extends AppCompatActivity {
    FragmentTransaction ft;
    ViewPager pager;
    TabLayout tabLayout;
    Fragment fragment;
    int currentPage=0;
    Button login,register;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_get_started);
        pager=findViewById(R.id.viewPager);
        tabLayout=findViewById(R.id.tabLayout);
        login=findViewById(R.id.login);
        register=findViewById(R.id.register);
        tabLayout.addTab(tabLayout.newTab().setIcon(R.drawable.tab_indicator_default));
        tabLayout.addTab(tabLayout.newTab().setIcon(R.drawable.tab_indicator_default));
        tabLayout.setTabGravity(TabLayout.GRAVITY_FILL);
        tabLayout.setupWithViewPager(pager,true);
        fragment=LiveOrderFragment.newInstance();
        ft=getSupportFragmentManager().beginTransaction();
        final int pageMargin = (int) TypedValue.applyDimension( TypedValue.COMPLEX_UNIT_DIP, 0, getResources() .getDisplayMetrics());
        GetStartedAdapter adapter=new GetStartedAdapter(getSupportFragmentManager(),tabLayout.getTabCount());

        pager.setPageMargin(pageMargin);
        pager.setAdapter(adapter);
        for(int i=0; i < tabLayout.getTabCount(); i++) {
            View tab = ((ViewGroup) tabLayout.getChildAt(0)).getChildAt(i);
            ViewGroup.MarginLayoutParams p = (ViewGroup.MarginLayoutParams) tab.getLayoutParams();
            p.setMargins(0, 0, 20, 0);
            tab.requestLayout();
        }
        pager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
        Handler handler=new Handler();
        final Runnable Update= new Runnable() {
            @Override
            public void run() {
                if(currentPage==2){
                    currentPage=0;
                }
                pager.setCurrentItem(currentPage++,true);
            }
        };

        login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(GetStartedActivity.this,LoginActivity.class));
            }
        });
        register.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(GetStartedActivity.this,RegisterUserActivity.class));
            }
        });
    }
}