package com.catnyx.store;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CartOrderCategoriesFragment extends Fragment {
    RecyclerView cart_items;
    CartCategoryAdapter adapter;
    List<Categories> categories;
    StringRequest request;
    ProgressBar loading;
    RequestQueue mqueue;
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView=inflater.inflate(R.layout.fragment_confirm_order,container,false);
        cart_items=rootView.findViewById(R.id.cart_items);
        loading=rootView.findViewById(R.id.loading);
        loading.setVisibility(View.VISIBLE);
        categories=new ArrayList<>();
        adapter=new CartCategoryAdapter(getActivity(),categories);
        mqueue=Volley.newRequestQueue(getActivity());
        cart_items.setLayoutManager(new LinearLayoutManager(getActivity()));
        cart_items.setAdapter(adapter);
        return rootView;
    }

    public static CartOrderCategoriesFragment newInstance() {

        Bundle args = new Bundle();

        CartOrderCategoriesFragment fragment = new CartOrderCategoriesFragment();
        fragment.setArguments(args);
        return fragment;
    }

    public void getCartCategories(){
        request=new StringRequest(Request.Method.GET, ConstantClass.CART_RESTAURANTS, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    JSONObject object = new JSONObject(response);
                    if (object.getString("message").equals("data found")) {
                    JSONArray array = object.getJSONArray("data");
                    categories.clear();
                    for (int i = 0; i < array.length(); i++) {
                        JSONObject res = array.getJSONObject(i);
                        categories.add(new Categories(res.getString("id"), res.getString("name"), ConstantClass.IMAGE_ACCESS + res.getString("image")));
                    }
                    adapter.notifyDataSetChanged();
                    loading.setVisibility(View.INVISIBLE);
                }
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
//                if(object.getString("message").equals("no data found")){
//                    Log.i("MESSAGE",object.getString("message"));
                    Fragment fragment=CartFragment.newInstance();
                    ((HomeActivity)getActivity()).createFragment(fragment);
//                }
            }
        }){
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String,String> map=new HashMap<>();
                map.put("Authorization","Bearer "+ConstantClass.TOKEN);
                return map;
            }
        };
        mqueue.add(request).setRetryPolicy(new RetryPolicy() {
            @Override
            public int getCurrentTimeout() {
                return 5000;
            }

            @Override
            public int getCurrentRetryCount() {
                return 5000;
            }

            @Override
            public void retry(VolleyError error) throws VolleyError {
                error.printStackTrace();
            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();
        getCartCategories();
        cart_items.setAdapter(adapter);
    }
}
