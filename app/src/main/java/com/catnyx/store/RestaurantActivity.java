package com.catnyx.store;

import android.Manifest;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.provider.Settings;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.cardview.widget.CardView;
import androidx.core.app.ActivityCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.bumptech.glide.Glide;
import com.google.android.material.appbar.AppBarLayout;
import com.google.android.material.appbar.CollapsingToolbarLayout;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class RestaurantActivity extends AppCompatActivity {
    private static final int REQUEST_LOCATION=1;
    LocationManager locationManager;

    CollapsingToolbarLayout collapsingToolbarLayout;
    AppBarLayout appBarLayout;
    Toolbar toolbar;
    RecyclerView itemsView;
    List<VendorClass> itemsList;
    TextView error;
    ImageView img;
    FloatingActionButton show_cart;
    String id,image;
    RequestQueue mqueue;
    NewVendorAdapter adapter;
    ProgressBar loading;
    String category_name;

    double latitude=27.928741;
    double longitude=78.092336;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_restaurant);
        ActivityCompat.requestPermissions(this,new String[]{
                Manifest.permission.ACCESS_FINE_LOCATION},REQUEST_LOCATION);
        collapsingToolbarLayout=findViewById(R.id.collapsingToolbar);
        appBarLayout=findViewById(R.id.appBar);
        toolbar=findViewById(R.id.toolbar);
        itemsView=findViewById(R.id.itemsView);
        show_cart=findViewById(R.id.add_to_cart);
        img=findViewById(R.id.img);
        loading=findViewById(R.id.loading);
        loading.setVisibility(View.VISIBLE);
        error=findViewById(R.id.error);
        error.setVisibility(View.INVISIBLE);
        mqueue= Volley.newRequestQueue(getApplicationContext());
        itemsList=new ArrayList<>();
        adapter=new NewVendorAdapter(RestaurantActivity.this,itemsList);
        itemsView.setAdapter(adapter);
        Intent intent=getIntent();
        id=intent.getStringExtra("id");
        image=intent.getStringExtra("image");
        category_name=intent.getStringExtra("category");
        ConstantClass.SUB_IMAGE=image;
        Glide.with(this)
                .load(image)
                .into(img);
        itemsView.setLayoutManager(new LinearLayoutManager(this));

        show_cart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(RestaurantActivity.this,HomeActivity.class);
//                ConstantClass.isCart=true;
                startActivity(intent);
            }
        });

        getWindow().setStatusBarColor(0xffff6d00);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        appBarLayout.addOnOffsetChangedListener(new AppBarLayout.OnOffsetChangedListener() {
            boolean isShow=true;
            int scrollRange=-1;
            @Override
            public void onOffsetChanged(AppBarLayout appBarLayout, int verticalOffset) {
                if(scrollRange == -1){
                    scrollRange=appBarLayout.getTotalScrollRange();
                }
                if(scrollRange + verticalOffset == 0){
                    collapsingToolbarLayout.setTitle("Product Info");
                    isShow=true;
                }else if(isShow){
                    collapsingToolbarLayout.setTitle(" ");
                    isShow=false;
                }
            }
        });
    }

    public void show_cart_btn(){
        show_cart.setVisibility(View.VISIBLE);
    }

    public void not_show(){
        show_cart.setVisibility(View.INVISIBLE);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()){
            case android.R.id.home:
                finish();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onResume() {
        super.onResume();
//        locationManager= (LocationManager) getSystemService(Context.LOCATION_SERVICE);
//        if (!locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER))
//        {
//            //Write Function To enable gps
//
//            OnGPS();
//        }
//        else
//        {
//            //GPS is already On then
//
//            getLocation();
//        }
//
//        getData();
    }

    private void OnGPS() {
        final AlertDialog alertDialog;
        final AlertDialog.Builder builder= new AlertDialog.Builder(this);
        builder.setMessage("Enable GPS").setCancelable(false).setPositiveButton("YES", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
                startActivity(new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS));

            }
        }).setNegativeButton("NO", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });
        alertDialog=builder.create();
        alertDialog.show();
    }



    private void getLocation() {

        //Check Permissions again

        if (ActivityCompat.checkSelfPermission(RestaurantActivity.this,Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(RestaurantActivity.this,

                Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED)
        {
            ActivityCompat.requestPermissions(this,new String[]
                    {Manifest.permission.ACCESS_FINE_LOCATION}, REQUEST_LOCATION);
        }
        else
        {
            Location LocationGps= locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
            Location LocationNetwork=locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
            Location LocationPassive=locationManager.getLastKnownLocation(LocationManager.PASSIVE_PROVIDER);

            if (LocationGps !=null)
            {
                double lat=LocationGps.getLatitude();
                double longi=LocationGps.getLongitude();

                latitude=lat;
                longitude=longi;
//                Log.i("ADDRESS:",lat+" : "+longi);
                //showLocationTxt.setText("Your Location:"+"\n"+"Latitude= "+latitude+"\n"+"Longitude= "+longitude);
            }
            else if (LocationNetwork !=null)
            {
                double lat=LocationNetwork.getLatitude();
                double longi=LocationNetwork.getLongitude();

                latitude=lat;
                longitude=longi;

//                Log.i("ADDRESS:",lat+" : "+longi);
             //   showLocationTxt.setText("Your Location:"+"\n"+"Latitude= "+latitude+"\n"+"Longitude= "+longitude);
            }
            else if (LocationPassive !=null)
            {
                double lat=LocationPassive.getLatitude();
                double longi=LocationPassive.getLongitude();

                latitude=lat;
                longitude=longi;

                Log.i("ADDRESS:",lat+" : "+longi);
               // showLocationTxt.setText("Your Location:"+"\n"+"Latitude= "+latitude+"\n"+"Longitude= "+longitude);
            }
            else
            {
//                Toast.makeText(this, "Can't Get Your Location", Toast.LENGTH_SHORT).show();
            }

            //Thats All Run Your App
        }

    }

    private void getData(){
        StringRequest request=new StringRequest(Request.Method.POST, ConstantClass.GET_VENDORS, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    JSONObject object=new JSONObject(response);
                    JSONArray array=object.getJSONArray("data");
                    Log.i("RES:",response);
                    itemsList.clear();
                    for(int i=0;i<array.length();i++){
                        JSONObject obj=array.getJSONObject(i);
                        if(obj.getString("category_name").equals(category_name)){
                            itemsList.add(new VendorClass(obj.getString("category_id"),obj.getString("name"),obj.getString("shop_name"),obj.getString("state"),obj.getString("city"),obj.getString("address"),ConstantClass.IMAGE_ACCESS+obj.getString("image")));
                        }
                    }
                    if(itemsList.size()==0){
                        Toast.makeText(getApplicationContext(),"No Vendors near you",Toast.LENGTH_SHORT).show();
                        error.setVisibility(View.VISIBLE);
                        loading.setVisibility(View.INVISIBLE);
                    }else{
                        error.setVisibility(View.INVISIBLE);
                    }
                    adapter.notifyDataSetChanged();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError err) {
                err.printStackTrace();
                error.setVisibility(View.VISIBLE);
                loading.setVisibility(View.INVISIBLE);
            }
        }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                HashMap<String,String> umap=new HashMap<>();
                umap.put("latitude", String.valueOf(latitude));
                umap.put("longitude", String.valueOf(longitude));
                return umap;
            }
        };
        mqueue.add(request).setRetryPolicy(new RetryPolicy() {
            @Override
            public int getCurrentTimeout() {
                return 5000;
            }

            @Override
            public int getCurrentRetryCount() {
                return 5000;
            }

            @Override
            public void retry(VolleyError error) throws VolleyError {
                error.printStackTrace();
            }
        });
    }


}