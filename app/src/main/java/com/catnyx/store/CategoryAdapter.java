package com.catnyx.store;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.core.app.ActivityOptionsCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;

import java.util.List;

public class CategoryAdapter extends RecyclerView.Adapter<CategoryAdapter.MyViewHolder> {
    Activity mContext;
    List<ItemClass> itemsList;

    public CategoryAdapter(Activity mContext, List<ItemClass> itemsList) {
        this.mContext = mContext;
        this.itemsList = itemsList;
    }

    @NonNull
    @Override
    public CategoryAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(parent.getContext()).inflate(R.layout.category_layout,parent,false);

        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final CategoryAdapter.MyViewHolder holder, final int position) {
        holder.name.setText(itemsList.get(position).getName());
        Glide.with(mContext)
                .load(itemsList.get(position).getImg())
                .into(holder.img);
        holder.restaurant_item.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(mContext,RestaurantInfoActivity.class);
                intent.putExtra("id",itemsList.get(position).getId());
                intent.putExtra("image",itemsList.get(position).getImg());
                ConstantClass.CATEGORY_NAME=itemsList.get(position).getName();
                ActivityOptionsCompat optionsCompat=ActivityOptionsCompat.makeSceneTransitionAnimation(mContext,holder.img,"icon");
                mContext.startActivity(intent,optionsCompat.toBundle());
            }
        });

//        holder.add.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                int i=itemsList.get(position).getNum()+1;
//                itemsList.get(position).setNum(i);
//                holder.count.setText(String.valueOf(i));
//            }
//        });
//        holder.minus.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                int j=itemsList.get(position).getNum()-1;
//                itemsList.get(position).setNum(j);
//                if(j<1 || j==0) {
//                    holder.counter.setVisibility(View.INVISIBLE);
//                    holder.adder.setVisibility(View.VISIBLE);
//                    ((RestaurantInfoActivity)mContext).not_show();
//                }else{
//                    holder.count.setText(String.valueOf(j));
//                }
//            }
//        });
    }

    @Override
    public int getItemCount() {
        return itemsList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView name,category;
        LinearLayout restaurant_item;
        ImageView img;
        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            name=itemView.findViewById(R.id.restaurant_name);
            category=itemView.findViewById(R.id.restaurant_category);
            img=itemView.findViewById(R.id.img);
            restaurant_item=itemView.findViewById(R.id.restaurant_item);
        }
    }
}
