package com.catnyx.store;

import android.app.Activity;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.core.app.ActivityOptionsCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;

import java.util.List;

public class NewVendorAdapter extends RecyclerView.Adapter<NewVendorAdapter.MyViewHolder> {
    Activity mContext;
    List<VendorClass> itemsList;

    public NewVendorAdapter(Activity mContext, List<VendorClass> itemsList) {
        this.mContext = mContext;
        this.itemsList = itemsList;
    }

    @NonNull
    @Override
    public NewVendorAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(parent.getContext()).inflate(R.layout.new_vendor_layout,parent,false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final NewVendorAdapter.MyViewHolder holder, final int position) {
        holder.name.setText(itemsList.get(position).getName());
        Glide.with(mContext)
                .load(itemsList.get(position).getImage())
                .into(holder.img);
        holder.item.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(mContext,CategoryActivity.class);
                intent.putExtra("id",itemsList.get(position).getId());
                intent.putExtra("image",itemsList.get(position).getImage());
                ConstantClass.CATEGORY_NAME=itemsList.get(position).getName();
                ActivityOptionsCompat optionsCompat=ActivityOptionsCompat.makeSceneTransitionAnimation(mContext,holder.img,"icon");
                mContext.startActivity(intent,optionsCompat.toBundle());
            }
        });

//        holder.add.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                int i=itemsList.get(position).getNum()+1;
//                itemsList.get(position).setNum(i);
//                holder.count.setText(String.valueOf(i));
//            }
//        });
//        holder.minus.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                int j=itemsList.get(position).getNum()-1;
//                itemsList.get(position).setNum(j);
//                if(j<1 || j==0) {
//                    holder.counter.setVisibility(View.INVISIBLE);
//                    holder.adder.setVisibility(View.VISIBLE);
//                    ((RestaurantInfoActivity)mContext).not_show();
//                }else{
//                    holder.count.setText(String.valueOf(j));
//                }
//            }
//        });
    }

    @Override
    public int getItemCount() {
        return itemsList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView name,address;
        LinearLayout item;
        ImageView img;
        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            name=itemView.findViewById(R.id.name);
            address=itemView.findViewById(R.id.address);
            img=itemView.findViewById(R.id.img);
            item=itemView.findViewById(R.id.item);
        }
    }
}
