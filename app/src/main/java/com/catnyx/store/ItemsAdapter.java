package com.catnyx.store;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.bumptech.glide.Glide;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ItemsAdapter extends RecyclerView.Adapter<ItemsAdapter.MyViewHolder> {
    Activity mContext;
    List<ItemClass> itemsList;
    SharedPreferences prefs;
    SharedPreferences.Editor editor;

    public ItemsAdapter(Activity mContext, List<ItemClass> itemsList) {
        this.mContext = mContext;
        this.itemsList = itemsList;
    }

    @NonNull
    @Override
    public ItemsAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        prefs=mContext.getSharedPreferences("user", Context.MODE_PRIVATE);
        editor=prefs.edit();
        View view= LayoutInflater.from(parent.getContext()).inflate(R.layout.item_details_layout,parent,false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final ItemsAdapter.MyViewHolder holder, final int position) {
        holder.name.setText(itemsList.get(position).getName());
        holder.cost.setText(itemsList.get(position).getCost());
        holder.qty.setText("per "+itemsList.get(position).getDuration());
        Glide.with(mContext)
                .load(itemsList.get(position).getImg())
                .into(holder.img);
        holder.category.setText(ConstantClass.CATEGORY_NAME);
        holder.adder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                holder.counter.setVisibility(View.VISIBLE);
//                holder.adder.setVisibility(View.INVISIBLE);
//                ((RestaurantInfoActivity)mContext).show_cart_btn();
//                Toast.makeText(mContext,"Item Added to cart!",Toast.LENGTH_SHORT).show();
                editor.putString("restaurant_id",ConstantClass.RESTAURANT_ID);
                editor.apply();
                addToCart(itemsList.get(position).getId());
            }
        });


//        holder.add.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                int i=itemsList.get(position).getNum()+1;
//                itemsList.get(position).setNum(i);
//                holder.count.setText(String.valueOf(i));
//            }
//        });
//        holder.minus.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                int j=itemsList.get(position).getNum()-1;
//                itemsList.get(position).setNum(j);
//                if(j<1 || j==0) {
//                    holder.counter.setVisibility(View.INVISIBLE);
//                    holder.adder.setVisibility(View.VISIBLE);
//                    ((RestaurantInfoActivity)mContext).not_show();
//                }else{
//                    holder.count.setText(String.valueOf(j));
//                }
//            }
//        });
    }

    @Override
    public int getItemCount() {
        return itemsList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView name,cost,qty,category;
        ImageView img;
        LinearLayout adder;
        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            name=itemView.findViewById(R.id.restaurant_name);
            qty=itemView.findViewById(R.id.time);
            cost=itemView.findViewById(R.id.cost);
            img=itemView.findViewById(R.id.img);
            adder=itemView.findViewById(R.id.adder);
            category=itemView.findViewById(R.id.restaurant_category);
        }
    }

    private void addToCart(final String id){
        RequestQueue mqueue= Volley.newRequestQueue(mContext);
        StringRequest request=new StringRequest(Request.Method.POST, ConstantClass.ADD_CART, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
//                    JSONObject object=new JSONObject(response);
//                    Log.i("CASES:",id+" : "+ConstantClass.TOKEN);
//                    Log.i("REST:",response);
//                    Log.i("ID",id);

                    JSONObject object=new JSONObject(response);

                    Toast.makeText(mContext,object.getString("message"),Toast.LENGTH_SHORT).show();
                    editor.putString("cart_available","yes");
                    editor.putString("cart_number",ConstantClass.RESTAURANT_ID);
                    editor.apply();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
            }
        }){

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String,String> map=new HashMap<>();
                map.put("Authorization","Bearer "+ConstantClass.TOKEN);
                return map;
            }

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                HashMap<String,String> umap=new HashMap<>();
                umap.put("product_id",id);
                return umap;
            }
        };
        mqueue.add(request).setRetryPolicy(new RetryPolicy() {
            @Override
            public int getCurrentTimeout() {
                return 5000;
            }

            @Override
            public int getCurrentRetryCount() {
                return 5000;
            }

            @Override
            public void retry(VolleyError error) throws VolleyError {

            }
        });
    }
}
