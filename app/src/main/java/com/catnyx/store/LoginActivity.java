package com.catnyx.store;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class LoginActivity extends AppCompatActivity {
    Button login;
    EditText username,pwd;
    RequestQueue mqueue;
    ProgressBar loading;
    TextView register;
    SharedPreferences prefs;
    SharedPreferences.Editor editor;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        login=findViewById(R.id.login);
        username=findViewById(R.id.username);
        pwd=findViewById(R.id.pwd);
        loading=findViewById(R.id.loading);
        register=findViewById(R.id.register);
        register.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(LoginActivity.this,RegisterUserActivity.class));
            }
        });
        loading.setVisibility(View.INVISIBLE);
        mqueue= Volley.newRequestQueue(getApplicationContext());
        prefs=getSharedPreferences("user",MODE_PRIVATE);
        editor=prefs.edit();
        login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(TextUtils.isEmpty(username.getText().toString())){
                    login.setError("Please enter email");
                    login.requestFocus();
                }else if(TextUtils.isEmpty(pwd.getText().toString())){
                    pwd.setError("Please enter password");
                    pwd.requestFocus();
                }else if(pwd.getText().toString().length()<8){
                    pwd.setError("Password not valid");
                    pwd.requestFocus();
                }else{
                    InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(login.getWindowToken(), 0);
                    login.setVisibility(View.INVISIBLE);
                    loading.setVisibility(View.VISIBLE);
                    loginApi();
                }
            }
        });
    }
    public void loginApi(){
        StringRequest request=new StringRequest(Request.Method.POST, ConstantClass.LOGIN, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
            Log.i("TAG","Success");
            try{
                JSONObject object=new JSONObject(response);
                JSONObject userlogin=object.getJSONObject("success");
                String token=userlogin.getString("token");
                JSONObject jsonObject=object.getJSONObject("data");
                String id=jsonObject.getString("id");
                String name=jsonObject.getString("name");
                String email=jsonObject.getString("email");
                editor.putString("email",email);
                editor.putString("token",token);
                editor.putString("name",name);
                editor.putString("id",id);
                editor.apply();
                ConstantClass.TOKEN=token;
                startActivity(new Intent(LoginActivity.this,HomeActivity.class));
            }catch (Exception e){
                e.printStackTrace();
            }
                loading.setVisibility(View.INVISIBLE);
                login.setVisibility(View.VISIBLE);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                loading.setVisibility(View.INVISIBLE);
                login.setVisibility(View.VISIBLE);
//                Log.i("STATUS","Error");
                Toast.makeText(getApplicationContext(),"Wrong!",Toast.LENGTH_SHORT).show();
            }
        }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                HashMap<String,String> umap=new HashMap<>();
                umap.put("email",username.getText().toString().trim());
                umap.put("password",pwd.getText().toString().trim());
                return umap;
            }
        };

        mqueue.add(request).setRetryPolicy(new RetryPolicy() {
            @Override
            public int getCurrentTimeout() {
                return 5000;
            }

            @Override
            public int getCurrentRetryCount() {
                return 5000;
            }

            @Override
            public void retry(VolleyError error) throws VolleyError {

            }
        });
    }
}