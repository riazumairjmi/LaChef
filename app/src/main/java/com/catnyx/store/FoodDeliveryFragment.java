package com.catnyx.store;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.bumptech.glide.Glide;

public class FoodDeliveryFragment extends Fragment {
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView=inflater.inflate(R.layout.fragment_food_delivery,container,false);
        ImageView img=rootView.findViewById(R.id.img);
        Glide.with(this)
                .load(R.drawable.bike)
                .into(img);
        return rootView;
    }

    public static FoodDeliveryFragment newInstance() {

        Bundle args = new Bundle();

        FoodDeliveryFragment fragment = new FoodDeliveryFragment();
        fragment.setArguments(args);
        return fragment;
    }
}
