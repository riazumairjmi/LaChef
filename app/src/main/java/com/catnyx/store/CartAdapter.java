package com.catnyx.store;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.bumptech.glide.Glide;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CartAdapter extends RecyclerView.Adapter<CartAdapter.MyViewHolder> {
    Activity mContext;
    List<BillDetailsClass> itemList;
//    Fragment fragment;
    SharedPreferences prefs;
    SharedPreferences.Editor editor;
    public CartAdapter(Activity mContext, List<BillDetailsClass> itemList) {
        this.mContext = mContext;
        this.itemList = itemList;
//        this.fragment=fragment;
    }

    @NonNull
    @Override
    public CartAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(parent.getContext()).inflate(R.layout.cart_item_layout,parent,false);
        prefs=mContext.getSharedPreferences("user", Context.MODE_PRIVATE);
        editor=prefs.edit();
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final CartAdapter.MyViewHolder holder, final int position) {
        holder.name.setText(itemList.get(position).getName());
        holder.qty.setText(itemList.get(position).getQty());
        Glide.with(mContext)
                .load(itemList.get(position).getImage())
                .into(holder.img);
        holder.cost.setText(itemList.get(position).getTotal_cost());
//        holder.category.setText(itemList.get(position).getQty());
        holder.add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int current=Integer.parseInt(holder.qty.getText().toString());
                if(current<10){
                    current+=1;
                }else{
                    Toast.makeText(mContext,"Max. reached",Toast.LENGTH_SHORT).show();
                }
                holder.qty.setText(String.valueOf(current));
                holder.cost.setText(String.valueOf(current*Integer.parseInt(itemList.get(position).getCost_one())));
                updateCart(position,current);
            }
        });
        holder.minus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int current=Integer.parseInt(holder.qty.getText().toString());
                if(current==1){
                    deleteItem(position);
                }else if(current>1){
                    current-=1;
                }
                holder.qty.setText(String.valueOf(current));
                holder.cost.setText(String.valueOf(current*Integer.parseInt(itemList.get(position).getCost_one())));
                updateCart(position,current);
            }
        });
        holder.delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                deleteItem(position);
            }
        });

//        holder.add_btn.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                int i=itemList.get(position).getNum()+1;
//                itemList.get(position).setNum(i);
//                holder.qty.setText(String.valueOf(i));
//            }
//        });
//        holder.minus_btn.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                int j=itemList.get(position).getNum()-1;
//                itemList.get(position).setNum(j);
//                if(j<1 || j==0) {
//
//                }else{
//                    holder.qty.setText(String.valueOf(j));
//                }
//            }
//        });
    }

    private void deleteItem(final int position){
        RequestQueue mqueue= Volley.newRequestQueue(mContext);
        StringRequest request=new StringRequest(Request.Method.POST, ConstantClass.REMOVE_CART_ITEM, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    JSONObject object=new JSONObject(response);
                    Toast.makeText(mContext,object.getString("message"),Toast.LENGTH_SHORT).show();
                    ((CartOrdersActivity)mContext).getCartItems();
                    if(position==itemList.size()){
                        itemList.clear();
                        editor.putString("cart_available","no");
                        editor.putString("cart_number","0");
                        editor.apply();
                    }else{
                        itemList.remove(position);
                    }
                    notifyDataSetChanged();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
            }
        }){

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                HashMap<String,String> umap=new HashMap<>();
                umap.put("item_id",itemList.get(position).getId());
                return umap;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String,String> map=new HashMap<>();
                map.put("Authorization","Bearer "+ConstantClass.TOKEN);
                return map;
            }
        };
        mqueue.add(request).setRetryPolicy(new RetryPolicy() {
            @Override
            public int getCurrentTimeout() {
                return 5000;
            }

            @Override
            public int getCurrentRetryCount() {
                return 5000;
            }

            @Override
            public void retry(VolleyError error) throws VolleyError {

            }
        });
    }

    private void updateCart(final int position, final int current){
        RequestQueue mqueue=Volley.newRequestQueue(mContext);
        StringRequest request=new StringRequest(Request.Method.POST, ConstantClass.UPDATE_CART, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    JSONObject object=new JSONObject(response);
                    Log.i("MESSSAGE",object.getString("message"));
                    ((CartOrdersActivity)mContext).getCartItems();

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
            }
        }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                HashMap<String,String> umap=new HashMap<>();
                umap.put("cart_id",itemList.get(position).getId());
                umap.put("quantity",String.valueOf(current));
                return umap;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String,String> map=new HashMap<>();
                map.put("Authorization","Bearer "+ConstantClass.TOKEN);
                return map;
            }
        };
        mqueue.add(request).setRetryPolicy(new RetryPolicy() {
            @Override
            public int getCurrentTimeout() {
                return 5000;
            }

            @Override
            public int getCurrentRetryCount() {
                return 5000;
            }

            @Override
            public void retry(VolleyError error) throws VolleyError {

            }
        });
    }

    @Override
    public int getItemCount() {
        return itemList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView name,cost,qty;
        ImageView img;
        Button delete,minus,add;
        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            name=itemView.findViewById(R.id.name);
            cost=itemView.findViewById(R.id.cost);
            img=itemView.findViewById(R.id.img);
            delete=itemView.findViewById(R.id.delete);
            minus=itemView.findViewById(R.id.minus);
            add=itemView.findViewById(R.id.add);
            qty=itemView.findViewById(R.id.qty);
        }
    }
}
