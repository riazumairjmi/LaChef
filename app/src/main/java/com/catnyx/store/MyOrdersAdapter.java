package com.catnyx.store;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

public class MyOrdersAdapter extends RecyclerView.Adapter<MyOrdersAdapter.MyViewHolder> {
    Activity mContext;
    List<BookedOrderClass> bookedList;

    public MyOrdersAdapter(Activity mContext, List<BookedOrderClass> bookedList) {
        this.mContext = mContext;
        this.bookedList = bookedList;
    }

    @NonNull
    @Override
    public MyOrdersAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(parent.getContext()).inflate(R.layout.my_order_layout_item,parent,false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MyOrdersAdapter.MyViewHolder holder, int position) {
        String content_new="";
        holder.status.setText("Status: "+bookedList.get(position).getStatus());
        holder.address.setText(bookedList.get(position).getAddress());
        holder.date.setText("Date: "+bookedList.get(position).getDate().substring(0,10));
        holder.total_cost.setText(bookedList.get(position).getTotal());
//        for(int i=0;i<bookedList.get(position).getItem_list().size();i++){
           content_new+=bookedList.get(position).getItem_list();
            Log.i("CONTEXT:",ConstantClass.USER_NAME);
//        }
        holder.contents.setText(content_new);
        holder.mode.setText("Payment Mode: "+bookedList.get(position).getMode());

        holder.name.setText(ConstantClass.USER_NAME);
    }

    @Override
    public int getItemCount() {
        return bookedList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView status,date,address,contents,total_cost,name,mode;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            status=itemView.findViewById(R.id.status);
            date=itemView.findViewById(R.id.date);
            address=itemView.findViewById(R.id.address);
            contents=itemView.findViewById(R.id.all_items);
            total_cost=itemView.findViewById(R.id.total_cost);
            name=itemView.findViewById(R.id.name);
            mode=itemView.findViewById(R.id.mode);
        }
    }
}
