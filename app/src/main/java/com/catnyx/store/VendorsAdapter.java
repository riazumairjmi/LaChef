package com.catnyx.store;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

public class VendorsAdapter extends RecyclerView.Adapter<VendorsAdapter.MyViewHolder> {
    Activity mContext;
    List<BillDetailsClass> billsList;

    public VendorsAdapter(Activity mContext, List<BillDetailsClass> billsList) {
        this.mContext = mContext;
        this.billsList = billsList;
    }

    @NonNull
    @Override
    public VendorsAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(parent.getContext()).inflate(R.layout.bill_item_layout,parent,false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull VendorsAdapter.MyViewHolder holder, int position) {
        holder.qty.setText(billsList.get(position).getQty());
        holder.cost_one.setText(billsList.get(position).getName());
        holder.cost_total.setText(billsList.get(position).getTotal_cost());
    }

    @Override
    public int getItemCount() {
        return billsList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView qty,cost_one,cost_total;
        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            qty=itemView.findViewById(R.id.qty);
            cost_one=itemView.findViewById(R.id.cost_one);
            cost_total=itemView.findViewById(R.id.cost_total);
        }
    }
}
