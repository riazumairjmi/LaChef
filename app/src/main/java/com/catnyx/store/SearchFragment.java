package com.catnyx.store;

import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class SearchFragment extends Fragment {
    RecyclerView list;
    List<ItemClass> searchList;
    List<String> search_text;
    EditText searchBar;
    TextView error;
    Toolbar searchToolbar;
    ArrayList<ItemClass> filtered_list;
    SearchQueryAdapter adapter;
    RequestQueue mqueue;
    StringRequest request;
    ProgressBar loading;
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView=inflater.inflate(R.layout.fragment_search,container,false);
        searchToolbar=rootView.findViewById(R.id.searchtoolbar);
        searchBar=rootView.findViewById(R.id.searchBar);

        mqueue= Volley.newRequestQueue(getActivity());

        searchList=new ArrayList<>();
        filtered_list=new ArrayList<>();
        search_text=new ArrayList<>();
        list=rootView.findViewById(R.id.list);
        error=rootView.findViewById(R.id.error);
        error.setVisibility(View.INVISIBLE);
        loading=rootView.findViewById(R.id.loading);
        loading.setVisibility(View.VISIBLE);

//        searchList.add("Apple");
//        searchList.add("Mi");
//        searchList.add("Samsung");
//        searchList.add("Vivo");
//        searchList.add("Realme");
//        searchList.add("Covers for Mobiles");
//        searchList.add("Screen Protectors");
//        searchList.add("Earphones");
//        searchList.add("Charging & other cables");

        adapter=new SearchQueryAdapter(getActivity(),searchList);
        list.setLayoutManager(new LinearLayoutManager(getActivity()));
        list.setAdapter(adapter);
        getAllItems();
        searchBar.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                filter(s.toString());
                getSearchResults(s.toString());
            }

            @Override
            public void afterTextChanged(Editable s) {
                filter(s.toString());
                getSearchResults(s.toString());
            }
        });

//        searchBar.setOnEditorActionListener(new TextView.OnEditorActionListener() {
//            @Override
//            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
//                if(actionId == KeyEvent.KEYCODE_ENTER){
//                    return true;
//                }else{
//                    Toast.makeText(getContext(),"CLICKED",Toast.LENGTH_SHORT).show();
//                }
//                return false;
//            }
//        });

        return rootView;
    }

    public static SearchFragment newInstance() {

        Bundle args = new Bundle();

        SearchFragment fragment = new SearchFragment();
        fragment.setArguments(args);
        return fragment;
    }

    public void filter(String text){
        loading.setVisibility(View.VISIBLE);
//        filtered_list.clear();
        for (String s : search_text){
            if(s.toLowerCase().contains(text.toLowerCase())){
//                filtered_list.add(s);
                getSearchResults(s);
            }
        }
        adapter.filterList(filtered_list);
    }

    private void getSearchResults(String s){
        request=new StringRequest(Request.Method.GET, ConstantClass.SEARCH_QUERY+s, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    JSONObject object=new JSONObject(response);
                    JSONArray array=object.getJSONArray("data");
//                    searchList.clear();
                    filtered_list.clear();
                    search_text.clear();
                    for (int i=0;i<array.length();i++){
                        JSONObject res=array.getJSONObject(i);
                        String id=res.getString("id");
                        String name=res.getString("product_name");
                        String image=ConstantClass.IMAGE_ACCESS+res.getString("image");
                        String cost=res.getString("rates");
                        String unit=res.getString("measurement_unit");
                        String time=res.getString("max_delivery_time");
                        filtered_list.add(new ItemClass(id,name,cost,unit,time,image));
                    }
                    loading.setVisibility(View.INVISIBLE);
                    error.setVisibility(View.INVISIBLE);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError err) {
                err.printStackTrace();
                loading.setVisibility(View.INVISIBLE);
                error.setVisibility(View.VISIBLE);
            }
        }){

        };
        mqueue.add(request).setRetryPolicy(new RetryPolicy() {
            @Override
            public int getCurrentTimeout() {
                return 5000;
            }

            @Override
            public int getCurrentRetryCount() {
                return 5000;
            }

            @Override
            public void retry(VolleyError error) throws VolleyError {

            }
        });
    }
    private void getAllItems(){
        request=new StringRequest(Request.Method.GET, ConstantClass.ALL_SEARCH, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    JSONObject object=new JSONObject(response);
                    JSONArray array=object.getJSONArray("data");
                    filtered_list.clear();
                    searchList.clear();
                    for(int i=0;i<array.length();i++){
                        JSONObject res=array.getJSONObject(i);
                        String id=res.getString("id");
                        String name=res.getString("product_name");
                        String image=ConstantClass.IMAGE_ACCESS+res.getString("image");
                        String cost=res.getString("rates");
                        String unit=res.getString("measurement_unit");
                        String time=res.getString("max_delivery_time");
                        search_text.add(name);
                        searchList.add(new ItemClass(id,name,cost,unit,time,image));
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError err) {

            }
        });
        mqueue.add(request).setRetryPolicy(new RetryPolicy() {
            @Override
            public int getCurrentTimeout() {
                return 5000;
            }

            @Override
            public int getCurrentRetryCount() {
                return 5000;
            }

            @Override
            public void retry(VolleyError error) throws VolleyError {

            }
        });
    }
}
