package com.catnyx.store;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

public class VendorOrdersFragment extends Fragment {
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView=inflater.inflate(R.layout.fragment_vendor_orders_list,container,false);
        return rootView;
    }

    public static VendorOrdersFragment newInstance() {

        Bundle args = new Bundle();

        VendorOrdersFragment fragment = new VendorOrdersFragment();
        fragment.setArguments(args);
        return fragment;
    }
}
