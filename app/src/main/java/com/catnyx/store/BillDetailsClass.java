package com.catnyx.store;

public class BillDetailsClass {
    String id;
    String image;
    String qty;
    String name;
    String total_cost;
    String cost_one;

    public BillDetailsClass(String id,String image,String qty, String name, String total_cost,String cost_one) {
        this.id=id;
        this.image=image;
        this.qty = qty;
        this.name = name;
        this.total_cost = total_cost;
        this.cost_one=cost_one;
    }

    public String getCost_one() {
        return cost_one;
    }

    public void setCost_one(String cost_one) {
        this.cost_one = cost_one;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getQty() {
        return qty;
    }

    public void setQty(String qty) {
        this.qty = qty;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTotal_cost() {
        return total_cost;
    }

    public void setTotal_cost(String total_cost) {
        this.total_cost = total_cost;
    }
}
