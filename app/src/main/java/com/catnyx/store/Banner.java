package com.catnyx.store;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.bumptech.glide.Glide;

public class Banner extends Fragment {
    private static final String ARG_RESOURCE_ID="resource_id";
    private String id;
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if(getArguments()!=null){
            id=getArguments().getString(ARG_RESOURCE_ID);
        }
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView=inflater.inflate(R.layout.banner,container,false);
        ImageView banner=rootView.findViewById(R.id.img1);
        Glide.with(this)
                .load(id)
                .into(banner);
        return rootView;
    }

    public static Banner newInstance(String id) {
        Bundle args = new Bundle();
        Banner fragment = new Banner();
        args.putString(ARG_RESOURCE_ID,id);
        fragment.setArguments(args);
        return fragment;
    }
}
