package com.catnyx.store;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.bumptech.glide.Glide;
import com.google.android.material.appbar.AppBarLayout;
import com.google.android.material.appbar.CollapsingToolbarLayout;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class RestaurantInfoActivity extends AppCompatActivity {
    CollapsingToolbarLayout collapsingToolbarLayout;
    AppBarLayout appBarLayout;
    Toolbar toolbar;
    RecyclerView itemsView;
    List<ItemClass> itemsList;
    ImageView img;
    FloatingActionButton show_cart;
    ProgressBar loading;
    String id;
    RequestQueue mqueue;
    ItemsAdapter adapter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_restaurant_info);
        collapsingToolbarLayout=findViewById(R.id.collapsingToolbar);
        appBarLayout=findViewById(R.id.appBar);
        toolbar=findViewById(R.id.toolbar);
        itemsView=findViewById(R.id.itemsView);
        show_cart=findViewById(R.id.add_to_cart);
        img=findViewById(R.id.img);
        loading=findViewById(R.id.loading);
        loading.setVisibility(View.VISIBLE);
        mqueue= Volley.newRequestQueue(getApplicationContext());
        itemsList=new ArrayList<>();
//        itemsList.add(new ItemClass("Samosa","12","Snacks","45",1,R.drawable.samosa));
//        itemsList.add(new ItemClass("Samosa","12","Snacks","45",1,R.drawable.samosa));
//        itemsList.add(new ItemClass("Samosa","12","Snacks","45",1,R.drawable.samosa));
//        itemsList.add(new ItemClass("Samosa","12","Snacks","45",1,R.drawable.samosa));

        adapter=new ItemsAdapter(RestaurantInfoActivity.this,itemsList);
        itemsView.setAdapter(adapter);
        Intent intent=getIntent();
        id=intent.getStringExtra("id");
        ConstantClass.SUB_IMAGE=intent.getStringExtra("image");
        Glide.with(this)
                .load(ConstantClass.SUB_IMAGE)
                .into(img);
        itemsView.setLayoutManager(new LinearLayoutManager(this));
        show_cart.setVisibility(View.VISIBLE);
        show_cart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(RestaurantInfoActivity.this,HomeActivity.class);
                ConstantClass.isCart=true;
                startActivity(intent);
            }
        });

        getWindow().setStatusBarColor(0xffff6d00);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        appBarLayout.addOnOffsetChangedListener(new AppBarLayout.OnOffsetChangedListener() {
            boolean isShow=true;
            int scrollRange=-1;
            @Override
            public void onOffsetChanged(AppBarLayout appBarLayout, int verticalOffset) {
                if(scrollRange == -1){
                    scrollRange=appBarLayout.getTotalScrollRange();
                }
                if(scrollRange + verticalOffset == 0){
                    collapsingToolbarLayout.setTitle("Product Info");
                    isShow=true;
                }else if(isShow){
                    collapsingToolbarLayout.setTitle(" ");
                    isShow=false;
                }
            }
        });
    }

//    public void show_cart_btn(){
//        show_cart.setVisibility(View.VISIBLE);
//    }

//    public void not_show(){
//        show_cart.setVisibility(View.INVISIBLE);
//    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()){
            case android.R.id.home:
                finish();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onResume() {
        super.onResume();
        StringRequest request=new StringRequest(Request.Method.GET, ConstantClass.ITEM+id, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    JSONObject object=new JSONObject(response);
                    JSONArray array=object.getJSONArray("data");
//                    Log.i("RESPONSE:",response);
                    itemsList.clear();
                    for(int i=0;i<array.length();i++){
                        JSONObject obj=array.getJSONObject(i);
                        itemsList.add(new ItemClass(obj.getString("id"),obj.getString("product_name"),obj.getString("rates"),"Quantity: "+obj.getString("measurement_unit"),obj.getString("max_delivery_time")+"min",ConstantClass.IMAGE_ACCESS+obj.getString("image")));
                    }
                    adapter.notifyDataSetChanged();
                    loading.setVisibility(View.INVISIBLE);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
            }
        });
        mqueue.add(request).setRetryPolicy(new RetryPolicy() {
            @Override
            public int getCurrentTimeout() {
                return 5000;
            }

            @Override
            public int getCurrentRetryCount() {
                return 5000;
            }

            @Override
            public void retry(VolleyError error) throws VolleyError {

            }
        });
    }
}