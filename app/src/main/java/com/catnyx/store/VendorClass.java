package com.catnyx.store;

public class VendorClass {
    String id;
    String name;
    String shop_name;
    String state;
    String city;
    String address;
    String image;

    public VendorClass(String id, String name, String shop_name, String state, String city, String address, String image) {
        this.id = id;
        this.name = name;
        this.shop_name = shop_name;
        this.state = state;
        this.city = city;
        this.address = address;
        this.image = image;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getShop_name() {
        return shop_name;
    }

    public void setShop_name(String shop_name) {
        this.shop_name = shop_name;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }
}
