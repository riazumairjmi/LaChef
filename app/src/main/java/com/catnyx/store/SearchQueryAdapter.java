package com.catnyx.store;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.bumptech.glide.Glide;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class SearchQueryAdapter extends RecyclerView.Adapter<SearchQueryAdapter.MyViewHolder> {
    private Activity mContext;
    private List<ItemClass> names;
    SharedPreferences prefs;
    SharedPreferences.Editor editor;

    public SearchQueryAdapter(Activity mContext, List<ItemClass> names){
        this.mContext=mContext;
        this.names=names;
    }

    @NonNull
    @Override
    public SearchQueryAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        prefs=mContext.getSharedPreferences("user", Context.MODE_PRIVATE);
        editor=prefs.edit();
        View view= LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_details_layout,parent,false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull SearchQueryAdapter.MyViewHolder holder, final int position) {
        holder.name.setText(names.get(position).getName());
        holder.cost.setText(names.get(position).getCost());
//        holder.time.setText(names.get(position).getDuration());
        Glide.with(mContext)
                .load(names.get(position).getImg())
                .into(holder.img);
        holder.category.setText(ConstantClass.CATEGORY_NAME);
        holder.adder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                holder.counter.setVisibility(View.VISIBLE);
//                holder.adder.setVisibility(View.INVISIBLE);
//                ((RestaurantInfoActivity)mContext).show_cart_btn();
//                Toast.makeText(mContext,"Item Added to cart!",Toast.LENGTH_SHORT).show();
                editor.putString("restaurant_id",ConstantClass.RESTAURANT_ID);
                editor.apply();
                addToCart(names.get(position).getId());
            }
        });
    }


    private void addToCart(final String id){
        RequestQueue mqueue= Volley.newRequestQueue(mContext);
        StringRequest request=new StringRequest(Request.Method.POST, ConstantClass.ADD_CART, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
//                    JSONObject object=new JSONObject(response);
//                    Log.i("CASES:",id+" : "+ConstantClass.TOKEN);
//                    Log.i("REST:",response);
//                    Log.i("ID",id);

                    JSONObject object=new JSONObject(response);

                    Toast.makeText(mContext,object.getString("message"),Toast.LENGTH_SHORT).show();
                    editor.putString("cart_available","yes");
                    editor.putString("cart_number",ConstantClass.RESTAURANT_ID);
                    editor.apply();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
            }
        }){

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String,String> map=new HashMap<>();
                map.put("Authorization","Bearer "+ConstantClass.TOKEN);
                return map;
            }

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                HashMap<String,String> umap=new HashMap<>();
                umap.put("product_id",id);
                return umap;
            }
        };
        mqueue.add(request).setRetryPolicy(new RetryPolicy() {
            @Override
            public int getCurrentTimeout() {
                return 5000;
            }

            @Override
            public int getCurrentRetryCount() {
                return 5000;
            }

            @Override
            public void retry(VolleyError error) throws VolleyError {

            }
        });
    }


    @Override
    public int getItemCount() {
        return names.size();
    }

    public void filterList(ArrayList<ItemClass> filterNames){
     this.names=filterNames;
     notifyDataSetChanged();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView name,cost,time,category;
        ImageView img;
        LinearLayout adder;
        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            name=itemView.findViewById(R.id.restaurant_name);
            time=itemView.findViewById(R.id.time);
            cost=itemView.findViewById(R.id.cost);
            img=itemView.findViewById(R.id.img);
            adder=itemView.findViewById(R.id.adder);
            category=itemView.findViewById(R.id.restaurant_category);
        }
    }
}
