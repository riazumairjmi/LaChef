package com.catnyx.store;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.widget.ImageView;

import com.bumptech.glide.Glide;

public class SplashScreenActivity extends AppCompatActivity {
    Handler handler;
    ImageView mainLogo;
    SharedPreferences prefs;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);
        prefs=getSharedPreferences("user",MODE_PRIVATE);
        mainLogo=findViewById(R.id.mainLogo);
        Glide.with(this)
                .load(R.drawable.bglachef)
                .into(mainLogo);
        handler=new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                if(!prefs.getString("token","no").equals("no")){
                    finish();
                    startActivity(new Intent(SplashScreenActivity.this,HomeActivity.class));
                }else{
                    finish();
                    startActivity(new Intent(SplashScreenActivity.this,GetStartedActivity.class));
                }
            }
        },3000);
    }
}