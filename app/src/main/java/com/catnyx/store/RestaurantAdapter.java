package com.catnyx.store;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.core.app.ActivityOptionsCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;

import java.util.List;

public class RestaurantAdapter extends RecyclerView.Adapter<RestaurantAdapter.MyViewHolder> {
    Activity mContext;
    List<RestaurantClass> restaurantList;

    public RestaurantAdapter(Activity mContext, List<RestaurantClass> restaurantList) {
        this.mContext = mContext;
        this.restaurantList = restaurantList;
    }

    @NonNull
    @Override
    public RestaurantAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(parent.getContext())
                .inflate(R.layout.restaurant_item,parent,false);

        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final RestaurantAdapter.MyViewHolder holder, final int position) {
        holder.name.setText(restaurantList.get(position).getName());
        holder.category.setText(restaurantList.get(position).getCategory());
        holder.offers.setText(restaurantList.get(position).getOffers());
        holder.rating.setText(restaurantList.get(position).getRating());
        holder.duration.setText(restaurantList.get(position).getDuration());
        Glide.with(mContext)
                .load(restaurantList.get(position).getImage())
                .into(holder.image);
        holder.item.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(mContext,RestaurantActivity.class);
                intent.putExtra("id",restaurantList.get(position).getId());
                intent.putExtra("image",restaurantList.get(position).getImage());
                intent.putExtra("category",restaurantList.get(position).getName());
                ConstantClass.RESTAURANT_ID=restaurantList.get(position).getId();
                ActivityOptionsCompat optionsCompat=ActivityOptionsCompat.makeSceneTransitionAnimation(mContext,holder.image,"icon");
                mContext.startActivity(intent,optionsCompat.toBundle());
            }
        });
    }

    @Override
    public int getItemCount() {
        return restaurantList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView name,category,offers,rating,duration;
        ImageView image;
        LinearLayout item;
        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            name=itemView.findViewById(R.id.restaurant_name);
            category=itemView.findViewById(R.id.restaurant_category);
            offers=itemView.findViewById(R.id.offers_title);
            rating=itemView.findViewById(R.id.rating);
            duration=itemView.findViewById(R.id.duration);
            image=itemView.findViewById(R.id.img);
            item=itemView.findViewById(R.id.restaurant_item);
        }
    }
}
