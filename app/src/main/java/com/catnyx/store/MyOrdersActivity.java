package com.catnyx.store;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ProgressBar;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class MyOrdersActivity extends AppCompatActivity {
    RecyclerView my_order_view;
    List<BookedOrderClass> booked_items;
//    List<Item> only_item;
    StringRequest request;
    ProgressBar loading;
    RequestQueue mqueue;
    MyOrdersAdapter adapter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_orders);
        my_order_view=findViewById(R.id.my_order_list_view);
        booked_items=new ArrayList<>();
//        only_item=new ArrayList<>();
        mqueue= Volley.newRequestQueue(getApplicationContext());
        getMyOrders();
        loading=findViewById(R.id.loading);
        loading.setVisibility(View.VISIBLE);
        adapter=new MyOrdersAdapter(MyOrdersActivity.this,booked_items);
        my_order_view.setLayoutManager(new LinearLayoutManager(this));
        my_order_view.setAdapter(adapter);
    }
    private void getMyOrders(){
        request=new StringRequest(Request.Method.GET, ConstantClass.MY_ORDERS, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
//                Log.i("NEW_RESPONSE",response);
                try {
                    JSONObject obj=new JSONObject(response);
                    JSONArray array=obj.getJSONArray("data");
                    booked_items.clear();
                    for(int i=0;i<array.length();i++){
                        JSONObject object=array.getJSONObject(i);
                        String id=object.getString("id");
                        String total=object.getString("net_total");
                        String address=object.getString("address");
                        String date=object.getString("date_placed");
                        String status=object.getString("order_status");
                        String email=object.getString("email");
                        JSONArray arr=object.getJSONArray("orderItem");
                        String items="";
                        String mode=object.getString("payment_mode");
//                        only_item.clear();
                        for(int j=0;j<arr.length();j++){
                            JSONObject item=arr.getJSONObject(j);
                            items+=item.getString("quantity")+" X "+item.getString("product_name")+" = "+item.getString("product_price")+"\n";
                        }
                        booked_items.add(new BookedOrderClass(id,total,address,email,date,status,items,mode));
                    }
                    adapter.notifyDataSetChanged();
                    loading.setVisibility(View.INVISIBLE);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
            }
        }){
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String,String> map=new HashMap<>();
                map.put("Authorization","Bearer "+ConstantClass.TOKEN);
                return map;
            }
        };
        mqueue.add(request).setRetryPolicy(new RetryPolicy() {
                                               @Override
                                               public int getCurrentTimeout() {
                                                   return 5000;
                                               }

                                               @Override
                                               public int getCurrentRetryCount() {
                                                   return 5000;
                                               }

                                               @Override
                                               public void retry(VolleyError error) throws VolleyError {

                                               }
                                           }
        );
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent intent=new Intent(MyOrdersActivity.this,HomeActivity.class);
        startActivity(intent);
    }
}