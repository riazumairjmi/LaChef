package com.catnyx.store;

public class RestaurantClass {
    String id;
    String name;
    String category;
    String offers;
    String rating;
    String duration;
    String image;

    public RestaurantClass(String id,String name, String category, String offers, String rating, String duration, String image) {
        this.id=id;
        this.name = name;
        this.category = category;
        this.offers = offers;
        this.rating = rating;
        this.duration = duration;
        this.image = image;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getOffers() {
        return offers;
    }

    public void setOffers(String offers) {
        this.offers = offers;
    }

    public String getRating() {
        return rating;
    }

    public void setRating(String rating) {
        this.rating = rating;
    }

    public String getDuration() {
        return duration;
    }

    public void setDuration(String duration) {
        this.duration = duration;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }
}
