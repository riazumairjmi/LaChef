package com.catnyx.store;

import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.ViewPager;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.material.tabs.TabLayout;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

public class HomeFragment extends Fragment {
    RecyclerView restaurant;
    RestaurantAdapter adapter;
    TextView number,testingdb;
    List<RestaurantClass> restaurantList;
    TabLayout tabLayout;
    ViewPager pager;
    int currentPage=0;
    Handler handler;
    Timer timer;
    RequestQueue mqueue;
    ProgressBar loading;
    List<String> banner_list;
    BannerAdapter banAdapter;
    DatabaseReference test;
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView=inflater.inflate(R.layout.fragment_home,container,false);
        restaurant=rootView.findViewById(R.id.restaurants);
        number=rootView.findViewById(R.id.number);
        pager=rootView.findViewById(R.id.pager);
        tabLayout=rootView.findViewById(R.id.tabDots);
        loading=rootView.findViewById(R.id.loading);
        loading.setVisibility(View.VISIBLE);
        mqueue= Volley.newRequestQueue(getActivity());
        restaurantList=new ArrayList<>();
        tabLayout.setTabGravity(TabLayout.GRAVITY_FILL);
        tabLayout.setupWithViewPager(pager,true);
        tabLayout.addTab(tabLayout.newTab().setIcon(R.drawable.tab_indicator_default));
        tabLayout.addTab(tabLayout.newTab().setIcon(R.drawable.tab_indicator_default));
        tabLayout.addTab(tabLayout.newTab().setIcon(R.drawable.tab_indicator_default));
        tabLayout.addTab(tabLayout.newTab().setIcon(R.drawable.tab_indicator_default));
        banner_list=new ArrayList<>();
        banAdapter=new BannerAdapter(getChildFragmentManager(),tabLayout.getTabCount(),banner_list);
        pager.setAdapter(banAdapter);
//        restaurantList.add(new RestaurantClass("LA CHEF","Multi Cuisine Restaurant","Flat 25% offer on all Orders","No Rating","45 MINS",R.drawable.main_logo));
//        restaurantList.add(new RestaurantClass("Lachef bakery","Lachef Bakery","Flat 0% offer on all Orders","No Rating","45 MINS",R.drawable.main_logo));
//        restaurantList.add(new RestaurantClass("LA chef Store","Best Groceries","Flat 25% offer on all Orders","No Rating","45 MINS",R.drawable.main_logo));
//        restaurantList.add(new RestaurantClass("LA CHEF","Multi Cuisine Restaurant","Flat 25% offer on all Orders","No Rating","45 MINS",R.drawable.main_logo));
        adapter=new RestaurantAdapter(getActivity(),restaurantList);
        restaurant.setAdapter(adapter);
        restaurant.setLayoutManager(new LinearLayoutManager(getContext()));
        handler=new Handler();
        final int pageMargin=(int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP,0,getResources().getDisplayMetrics());
        pager.setPageMargin(pageMargin);
        for(int i=0;i<tabLayout.getTabCount();i++){
            View tab = ((ViewGroup) tabLayout.getChildAt(0)).getChildAt(i);
            ViewGroup.MarginLayoutParams p = (ViewGroup.MarginLayoutParams) tab.getLayoutParams();
            p.setMargins(20, 0, 20, 0);
            tab.requestLayout();
        }
        pager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
        final Runnable Update=new Runnable() {
            @Override
            public void run() {
                if(currentPage==4){
                    currentPage=0;
                }
                pager.setCurrentItem(currentPage++,true);
            }
        };
        timer=new Timer();
        timer.schedule(new TimerTask() {
            @Override
            public void run() {
                handler.post(Update);
            }
        },3000,2000);
        return rootView;
    }

    public static HomeFragment newInstance() {
        
        Bundle args = new Bundle();
        
        HomeFragment fragment = new HomeFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onResume() {
        super.onResume();
        StringRequest request=new StringRequest(Request.Method.GET, ConstantClass.RESTAURANTS, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    JSONObject object=new JSONObject(response);
                    JSONArray array=object.getJSONArray("data");
                    restaurantList.clear();
                    for(int i=0;i<array.length();i++){
                        JSONObject obj=array.getJSONObject(i);
                        restaurantList.add(new RestaurantClass(obj.getString("id"),obj.getString("name"),"veg","20% off on all orders","5","45min",ConstantClass.IMAGE_ACCESS+obj.getString("image")));
                    }
                    adapter.notifyDataSetChanged();
                    loading.setVisibility(View.INVISIBLE);
                    number.setText(restaurantList.size()+" CATEGORIES");
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
            }
        });
        mqueue.add(request).setRetryPolicy(new RetryPolicy() {
            @Override
            public int getCurrentTimeout() {
                return 5000;
            }

            @Override
            public int getCurrentRetryCount() {
                return 5000;
            }

            @Override
            public void retry(VolleyError error) throws VolleyError {

            }
        });
        getBanners();
    }

    private void getBanners(){
        StringRequest request=new StringRequest(Request.Method.GET, ConstantClass.BANNER_URL, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    JSONObject object=new JSONObject(response);
                    JSONArray array=object.getJSONArray("data");
                    int size=array.length();
                    if(size<4){
                        size=array.length();
                    }else{
                        size=4;
                    }
//                    Log.i("BANNERS:",array.length()+"size");
                    banner_list.clear();
                    for (int i=0;i<4;i++){
                        banner_list.add(ConstantClass.IMAGE_ACCESS+"/uploads/banners/5f6ad007595af1600835591.5912.jpg");
                    }
                    for(int i=0;i<size;i++){
                        JSONObject obj=array.getJSONObject(i);
                        String banner=ConstantClass.IMAGE_ACCESS+obj.getString("banner");
//                        Log.i("BANNERS:",banner);
                        banner_list.set(i,banner);
                    }
                    banAdapter.notifyDataSetChanged();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
            }
        });
        mqueue.add(request).setRetryPolicy(new RetryPolicy() {
            @Override
            public int getCurrentTimeout() {
                return 5000;
            }

            @Override
            public int getCurrentRetryCount() {
                return 5000;
            }

            @Override
            public void retry(VolleyError error) throws VolleyError {
                error.printStackTrace();
            }
        });
    }
}
