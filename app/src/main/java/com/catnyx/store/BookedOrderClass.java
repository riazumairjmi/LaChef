package com.catnyx.store;

import java.util.List;

public class BookedOrderClass {
    String id;
    String total;
    String address;
    String email;
    String date;
    String status;
    String item_list;
    String mode;

    public BookedOrderClass(String id, String total, String address, String email, String date,String status,String item_list,String mode) {
        this.id = id;
        this.total = total;
        this.address = address;
        this.email = email;
        this.date = date;
        this.status=status;
        this.item_list=item_list;
        this.mode=mode;
    }

    public String getMode() {
        return mode;
    }

    public void setMode(String mode) {
        this.mode = mode;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getItem_list() {
        return item_list;
    }

    public void setItem_list(String item_list) {
        this.item_list = item_list;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTotal() {
        return total;
    }

    public void setTotal(String total) {
        this.total = total;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }
}
