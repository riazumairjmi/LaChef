package com.catnyx.store;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;

public class GetStartedAdapter extends FragmentStatePagerAdapter {
    int tabCount;
    public GetStartedAdapter(@NonNull FragmentManager fm,int tabCount) {
        super(fm);
        this.tabCount=tabCount;
    }

    @NonNull
    @Override
    public Fragment getItem(int position) {
        switch (position){
            case 0: LiveOrderFragment tab1=new LiveOrderFragment();
            return tab1;
            case 1: FoodDeliveryFragment tab2=new FoodDeliveryFragment();
            return tab2;
            default:
                return null;
        }
    }

    @Override
    public int getCount() {
        return 2;
    }
}
