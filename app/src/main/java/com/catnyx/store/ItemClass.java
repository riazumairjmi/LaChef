package com.catnyx.store;

public class ItemClass {
    String name;                    //
    String cost;                  //
    String category;
    String duration;
//    int num;
    String id;                 //
    String img;                 //

    public ItemClass(String id,String name, String cost, String category, String duration, String img) {
        this.id=id;
        this.name = name;
        this.cost = cost;
        this.category = category;
        this.duration = duration;
//        this.num=num;
        this.img = img;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

//    public int getNum() {
//        return num;
//    }

//    public void setNum(int num) {
//        this.num = num;
//    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCost() {
        return cost;
    }

    public void setCost(String cost) {
        this.cost = cost;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getDuration() {
        return duration;
    }

    public void setDuration(String duration) {
        this.duration = duration;
    }

    public String getImg() {
        return img;
    }

    public void setImg(String img) {
        this.img = img;
    }
}
