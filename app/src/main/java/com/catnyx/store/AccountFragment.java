package com.catnyx.store;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.text.InputType;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class AccountFragment extends Fragment {
    RequestQueue mqueue;
    TextView name,email,logout;
    SharedPreferences prefs;
    TextView edit_btn,my_orders;
    EditText state,city,zip,address;
    Button save_btn;
    SharedPreferences.Editor editor;
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView=inflater.inflate(R.layout.fragment_account,container,false);
        mqueue= Volley.newRequestQueue(getContext());
        name=rootView.findViewById(R.id.name);
        email=rootView.findViewById(R.id.email);
        address=rootView.findViewById(R.id.address);
        state=rootView.findViewById(R.id.state);
        city=rootView.findViewById(R.id.city);
        zip=rootView.findViewById(R.id.zip);
        address=rootView.findViewById(R.id.address);
        save_btn=rootView.findViewById(R.id.save_btn);
        my_orders=rootView.findViewById(R.id.textView5);
        logout=rootView.findViewById(R.id.logout);

        save_btn.setVisibility(View.INVISIBLE);
//        address.setEnabled(false);
//        address.setInputType(InputType.TYPE_NULL);
//        address.setFocusable(false);
        edit_btn=rootView.findViewById(R.id.edit_btn);
        edit_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                address.setEnabled(true);
//                address.setInputType(InputType.TYPE_CLASS_TEXT);
//                address.setFocusable(true);
                save_btn.setVisibility(View.VISIBLE);
            }
        });
        save_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                saveAddress();
            }
        });
        prefs=this.getActivity().getSharedPreferences("user", Context.MODE_PRIVATE);
        editor=prefs.edit();
        name.setText(prefs.getString("name","name"));
        ConstantClass.USER_NAME=prefs.getString("name","name");
        Log.i("USERNAME",prefs.getString("name","name"));
        my_orders.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getActivity(),MyOrdersActivity.class));
            }
        });
        logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                editor.putString("token","no");
                editor.apply();
                Intent intent=new Intent(getActivity(),LoginActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
            }
        });
        return rootView;
    }

    public static AccountFragment newInstance() {

        Bundle args = new Bundle();

        AccountFragment fragment = new AccountFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onResume() {
        super.onResume();
        StringRequest request=new StringRequest(Request.Method.GET, ConstantClass.USER_ACCOUNT, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    JSONObject object=new JSONObject(response);
                    JSONObject data=object.getJSONObject("data");
                    String get_email=data.getString("email");
                    String contact=data.getString("contact");
//                    Log.i("ADD:",address.toString());
//                    Log.i("USER",response);
                    editor.putString("mobile",data.getString("contact"));
                    editor.putString("email",data.getString("email"));
                    email.setText(get_email);
//                    JSONObject add=data.getJSONObject("address");
//                    if(!address.equals("null")){
                        JSONObject obj=data.getJSONObject("address");
                        state.setText(obj.getString("state"));
                        city.setText(obj.getString("city"));
                        zip.setText(obj.getString("zip"));
                        address.setText(obj.getString("address"));
                        editor.putString("state",obj.getString("state"));
                        editor.putString("city",obj.getString("city"));
                        editor.putString("zip",obj.getString("zip"));
                        editor.putString("address",obj.getString("address"));
                        editor.apply();
//                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
            }
        }){
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String,String> map=new HashMap<>();
                map.put("Authorization","Bearer "+ConstantClass.TOKEN);
                return map;
            }
        };
        mqueue.add(request).setRetryPolicy(new RetryPolicy() {
            @Override
            public int getCurrentTimeout() {
                return 5000;
            }

            @Override
            public int getCurrentRetryCount() {
                return 5000;
            }

            @Override
            public void retry(VolleyError error) throws VolleyError {

            }
        });
    }

    public void saveAddress(){
    StringRequest request=new StringRequest(Request.Method.POST, ConstantClass.ADD_ADDRESS, new Response.Listener<String>() {
        @Override
        public void onResponse(String response) {
//            Log.i("RES:",response);
//            Log.i("RESADD:",state.getText().toString()+" : "+city.getText().toString()+" : "+zip.getText().toString()+" : "+address.getText().toString());
            editor.putString("state",state.getText().toString());
            editor.putString("city",city.getText().toString());
            editor.putString("zip",zip.getText().toString());
            editor.putString("address",address.getText().toString());
            editor.apply();
        }
    }, new Response.ErrorListener() {
        @Override
        public void onErrorResponse(VolleyError error) {
            error.printStackTrace();
        }
    }){

        @Override
        public Map<String, String> getHeaders() throws AuthFailureError {
            HashMap<String,String> map=new HashMap<>();
            map.put("Authorization","Bearer "+ConstantClass.TOKEN);
            return map;
        }

        @Override
        protected Map<String, String> getParams() throws AuthFailureError {
            HashMap<String,String> umap=new HashMap<>();
            umap.put("state",state.getText().toString().trim());
            umap.put("city",city.getText().toString().trim());
            umap.put("zip",zip.getText().toString().trim());
            umap.put("address",address.getText().toString().trim());
            return umap;
        }
    };
    mqueue.add(request).setRetryPolicy(new RetryPolicy() {
        @Override
        public int getCurrentTimeout() {
            return 5000;
        }

        @Override
        public int getCurrentRetryCount() {
            return 5000;
        }

        @Override
        public void retry(VolleyError error) throws VolleyError {

        }
    });
    }
}
