package com.catnyx.store;

import android.media.Image;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.bumptech.glide.Glide;

public class LiveOrderFragment extends Fragment {
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView=inflater.inflate(R.layout.fragment_live_order,container,false);
        ImageView img=rootView.findViewById(R.id.img);
        Glide.with(this)
                .load(R.drawable.location)
                .into(img);
        return rootView;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    public static LiveOrderFragment newInstance() {

        Bundle args = new Bundle();

        LiveOrderFragment fragment = new LiveOrderFragment();
        fragment.setArguments(args);
        return fragment;
    }
}
