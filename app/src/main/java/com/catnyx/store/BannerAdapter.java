package com.catnyx.store;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;

import java.util.List;

public class BannerAdapter extends FragmentStatePagerAdapter {
//    private static String[] IMAGES=ConstantClass.BANNER_IMAGES;
    int tab_count;
    List<String> IMAGES;
    public BannerAdapter(@NonNull FragmentManager fm,int tab_count,List<String> IMAGES) {
        super(fm);
        this.tab_count=tab_count;
        this.IMAGES=IMAGES;
    }

    @NonNull
    @Override
    public Fragment getItem(int position) {
        if(position<IMAGES.size()){
            return Banner.newInstance(IMAGES.get(position));
        }
        return null;
    }

    @Override
    public int getCount() {
        return IMAGES.size();
    }
}
