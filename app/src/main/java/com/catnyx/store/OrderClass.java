package com.catnyx.store;

public class OrderClass {
    String id,name,total,mobile,address,items,mode;
    String orderList;

    public OrderClass(String id, String name, String total, String mobile, String address, String items, String mode, String orderList) {
        this.id = id;
        this.name = name;
        this.total = total;
        this.mobile = mobile;
        this.address = address;
        this.items = items;
        this.mode = mode;
        this.orderList=orderList;
    }

    public String getOrderList() {
        return orderList;
    }

    public void setOrderList(String orderList) {
        this.orderList = orderList;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTotal() {
        return total;
    }

    public void setTotal(String total) {
        this.total = total;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getItems() {
        return items;
    }

    public void setItems(String items) {
        this.items = items;
    }

    public String getMode() {
        return mode;
    }

    public void setMode(String mode) {
        this.mode = mode;
    }
}
